<?php
/**
 * Plugin Name: Telegram Notifier V2
 * Plugin URI: some href
 * Description: <strong>some description</strong>
 * Version: 0.1
 * Author: Stos Dima
 * Author URI: stosdima@gmail.com
 * License: MIT
 */

use TelegramNotifier\ServiceContainer\Loader;
use TelegramNotifier\TelegramBot;

$pluginUrl = plugin_dir_path(__FILE__);
//Composer path
require_once($_SERVER['DOCUMENT_ROOT'] .'/vendor/autoload.php');
//From archive
//require_once('vendor/autoload.php');


require_once($pluginUrl . '/config.php');

class TelegramNotifier
{
    public function __construct()
    {
        if (is_admin()) {
            $settingsPage = new \TelegramNotifier\TelegramMenu();
            $db = Loader::resolve('db');
            register_activation_hook(__FILE__, [$db, 'create_table']);
            register_deactivation_hook(__FILE__, [$db, 'delete_table']);
        }
        $bot = new TelegramBot();
    }
}

$plugin = new TelegramNotifier();