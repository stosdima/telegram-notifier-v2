<?php

namespace TelegramNotifier\TelegramBot;


use TelegramBot\Api\Exception;
use TelegramBot\Api\Types\Inline\QueryResult\Photo;
use TelegramBot\Api\Types\Message;
use TelegramBot\Api\Types\Update;
use TelegramNotifier\ServiceContainer\Loader;
use TelegramNotifier\TelegramBot\Commands\CommandBus;
use TelegramNotifier\TelegramBot\Commands\IncomingMessagesHandler;

class CommandProcessor
{
    /**Telegram Bot Api
     * @var \TelegramBot\Api\Client $client
     */
    protected $client;

    /**
     * @var null $commandBus
     */
    protected $commandBus = null;

    /**
     * @var $offset
     */
    protected $offset;

    public function __construct(\TelegramBot\Api\Client $client)
    {
        $this->client = $client;
    }

    /**Getting CommandsBus
     * @return null|CommandBus
     * @throws \Exception
     */
    public function getCommandBus()
    {
        if (is_null($this->commandBus)) {
            return $this->commandBus = new CommandBus($this->client, $this);
        }
        return $this->commandBus;
    }

    /**Handle commands from Update
     * @param Update $update
     * @throws \Exception
     */
    private function processCommands(Update $update)
    {
        $message = $update->getMessage();
        $callbackQuery = $update->getCallbackQuery();
        if ($message !== null && $message->getText()) {
            $this->getCommandBus()->handler($message->getText(), $update);
        } elseif ($callbackQuery !== null && $callbackQuery->getData()) {
            $this->getCommandBus()->handler($callbackQuery->getData(), $update);
        }
    }

    /**Incoming commands and messsages handler
     * @param bool $webhook
     * @throws \Exception
     */
    public function commandsHandler($webhook = true)
    {
        if (!$webhook) {
            $this->offset = 0;
            $updates = $this->client->getUpdates($this->offset, 60);
            foreach ($updates as $update) {
                $this->offset = $updates[count($updates) - 1]->getUpdateId() + 1;
                $this->processCommands($update);
            }
            //Processing incoming messages from Update
            (new IncomingMessagesHandler())->make($this->client, null, new Update(), $this);
            $this->client->handle($updates);
            $updates = $this->client->getUpdates($this->offset, 60);
        } else {
            if ($webhookUpdate = \TelegramBot\Api\BotApi::jsonValidate($this->client->getRawBody(), true)) {
                $this->processCommands(Update::fromResponse($webhookUpdate));
                //Processing incoming messages from Update
                (new IncomingMessagesHandler())->make($this->client, null, Update::fromResponse($webhookUpdate), $this);
                $this->client->run();
            }
        }
    }

    /**Command adder
     * @param $name
     * @throws \Exception
     * @throws \TelegramNotifier\Exception\BotCommandException
     */
    public function addCommand($name)
    {
        $this->getCommandBus()->addCommand($name);
    }

    /**Commands adder
     * @param $names
     * @throws \Exception
     */
    public function addCommands($names)
    {
        $this->getCommandBus()->addCommands($names);
    }

    /** Get exists commands
     * @return array
     * @throws \Exception
     */
    public function getCommands()
    {
        return $this->getCommandBus()->getCommands();
    }
}