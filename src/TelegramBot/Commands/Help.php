<?php

namespace TelegramNotifier\TelegramBot\Commands;


use TelegramNotifier\ServiceContainer\Loader;

class Help extends Command
{
    protected $name = 'help';

    protected $description = 'Show command`s list';

    public function handle($arguments)
    {
        $commands = $this->getCommands();
        $text = '';
        foreach ($commands as $name => $handler) {
            $text .= sprintf('/%s - %s' . PHP_EOL, $name, $handler->getDescription());
        }
        $client = $this->client;
        $client->command($this->getName(), function ($message) use ($client, $text) {
            $client->sendMessage($message->getChat()->getId(), $text);
        });
    }
}