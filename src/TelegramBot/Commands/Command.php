<?php

namespace TelegramNotifier\TelegramBot\Commands;


use TelegramBot\Api\Client;
use TelegramBot\Api\Types\Update;
use TelegramNotifier\TelegramBot\CommandProcessor;

abstract class Command implements CommandInterface
{
    /** Command name
     * @var $name
     */
    protected $name;

    /**Api
     * @var $client \TelegramBot\Api\Client
     */
    protected $client;

    /** Command description
     * @var $description
     */
    protected $description;

    /** Update
     * @var $update \TelegramBot\Api\Types\Update
     */
    protected $update;

    /** Command arguments
     * @var $arguments
     */
    protected $arguments;

    /**
     * @var $processor CommandProcessor
     */
    protected $processor;

    /** Get command name
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /** Get command description
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**Process inbound command
     * @param Client $client
     * @param $arguments
     * @param Update $update
     * @param CommandProcessor $processor
     * @return mixed
     */
    public function make(Client $client, $arguments, Update $update, CommandProcessor $processor)
    {
        $this->client = $client;
        $this->arguments = $arguments;
        $this->update = $update;
        $this->processor = $processor;
        return $this->handle($arguments);
    }

    /**
     * @return null|CommandBus
     * @throws \Exception
     */
    public function getCommandBus()
    {
        return $this->processor->getCommandBus();
    }

    /**Get array of commands from @see CommandProcessor
     * @return array
     * @throws \Exception
     */
    public function getCommands()
    {
        return $this->processor->getCommands();
    }

    /** Process the command
     * @param $arguments
     * @return mixed
     */
    abstract public function handle($arguments);
}