<?php

namespace TelegramNotifier\TelegramBot\Commands;


use TelegramNotifier\ServiceContainer\Loader;

class Stop extends Command
{
    protected $name = 'stop';

    protected $description = 'Stop getting notifications from bot';

    public function handle($arguments)
    {
        $client = $this->client;
        $db = Loader::resolve('db');
        $client->command($this->getName(), function ($message) use ($client, $db) {
            $db->deleteContact($message->getChat()->getId());
            $text = 'You have been deleted from bot database. If you want start again, please, send me /start';
            $client->sendMessage($message->getChat()->getId(), $text);
        });
    }
}