<?php

namespace TelegramNotifier\TelegramBot\Commands;


use TelegramBot\Api\Types\Message;
use TelegramNotifier\ServiceContainer\Loader;

class Start extends Command
{
    protected $name = 'start';

    protected $description = 'Start getting notifications from bot';

    public function handle($arguments)
    {
        $client = $this->client;
        $db = Loader::resolve('db');
        $client->command($this->getName(), function (Message $message) use ($client, $db) {
            $db->addContact($message->getChat()->getId());
            $db->resetStatus($message->getChat()->getId());
            $text = 'Hello, thank`s for subscribing. Commands list: /help';
            $client->sendMessage($message->getChat()->getId(), $text);
        });
    }
}