<?php

namespace TelegramNotifier\TelegramBot\Commands;


use TelegramBot\Api\Types\Update;
use TelegramNotifier\Exception\BotCommandException;
use TelegramNotifier\ServiceContainer\Loader;
use TelegramNotifier\TelegramBot\CommandProcessor;
use TelegramNotifier\TelegramBot\Commands\CommandInterface;

class CommandBus
{
    /** List of commands
     * @var array $commands
     */
    public $commands = [];

    /** Api
     * @var \TelegramBot\Api\Client $client
     */
    protected $client;

    protected $processor;

    public function __construct(\TelegramBot\Api\Client $client, CommandProcessor $commandProcessor)
    {
        $this->client = $client;
        $this->processor = $commandProcessor;
    }

    /**Get commands
     * @return array
     */
    public function getCommands()
    {
        return $this->commands;
    }

    /**Add single command
     * @param $command
     * @return $this
     * @throws BotCommandException
     * @throws \Exception
     */
    public function addCommand($command)
    {
        if (!is_object($command)) {
            if (!class_exists($command)) {
                throw new BotCommandException(sprintf(
                    'Command class "%s" not found! Please make sure the class exists.',
                    $command
                ));
            }
            Loader::register($command, function () use ($command) {
                return new $command;
            });
            $command = Loader::resolve($command);
        }

        if ($command instanceof CommandInterface) {
            $this->commands[$command->getName()] = $command;
            return $this;
        }
        throw new BotCommandException('Command class should be an instance of "TelegramNotifier\TelegramBot\CommandInterface"');

    }

    /**Add commands
     * @param array $commands
     * @throws BotCommandException
     * @throws \Exception
     */
    public function addCommands(array $commands)
    {
        foreach ($commands as $command) {
            $this->addCommand($command);
        }
    }

    /**Remove single command
     * @param $name
     * @return $this
     */
    public function removeCommand($name)
    {
        unset($this->commands[$name]);
        return $this;
    }

    /**Remove commands
     * @param array $names
     * @return $this
     */
    public function removeCommands(array $names)
    {
        foreach ($names as $name) {
            $this->removeCommand($name);
        }
        return $this;
    }

    /**Handling command or message from Update
     * @param $message
     * @param Update $update
     * @return Update
     */
    public function handler($message, Update $update)
    {
        $match = $this->parseCommand($message);
        $messageHandler = new IncomingMessagesHandler();
        if (!empty($match)) {
            $command = $match[1];
            $arguments = $match[3];
            $this->execute($command, $arguments, $update);
        }
        return $update;
    }

    /**Execute command
     * @param $name
     * @param $arguments
     * @param $message
     * @return mixed
     */
    public function execute($name, $arguments, $message)
    {
        if (array_key_exists($name, $this->commands)) {
            return $this->commands[$name]->make($this->client, $arguments, $message, $this->processor);
        }
        return 'Ok';
    }

    /**Parse icoming command from Update
     * @param $text
     * @return mixed
     */
    public function parseCommand($text)
    {
        if (trim($text) === '') {
            throw new \InvalidArgumentException('Message is empty, Cannot parse for command');
        }

        preg_match('/^\/([^\s@]+)@?(\S+)?\s?(.*)$/', $text, $matches);

        return $matches;
    }
}