<?php

namespace TelegramNotifier\TelegramBot\Commands;


use TelegramBot\Api\Types\Message;
use TelegramBot\Api\Types\Update;
use TelegramNotifier\ServiceContainer\Loader;

class IncomingMessagesHandler extends Command
{
    public function handle($arguments)
    {
        $client = $this->client;
        $db = Loader::resolve('db');
        $helper = Loader::resolve('helper');
        $client->on(function (Update $update) use ($client, $db, $helper) {
            $chat_id = $update->getMessage()->getChat()->getId();
            $userText = $update->getMessage()->getText();
            $userStatus = $db->getStatus($chat_id);
            switch ($userStatus) {
                case 'admin-verif':
                    if (TELEGRAM_VERIFICATION_CODE == $userText) {
                        $text = 'You are logged in. Thanks!';
                        $db->updateAdmin($chat_id);
                        $db->resetStatus($chat_id);
                        $client->sendMessage($chat_id, $text, 'html');
                    } else {
                        $text = 'Incorrect verification code. Please re-type: ';
                        $client->sendMessage($chat_id, $text, 'html');
                    }
                    break;
                case 'admin-post-delete':
                    if ($separetedText = explode(',', $userText)) {
                        foreach ($separetedText as $text) {
                            if (wp_delete_post($text)) {
                                $text = "Post with ID: $text was deleted.";
                                $client->sendMessage($chat_id, $text, 'html');
                            } else {
                                $text = 'Error. Post not found:';
                                $client->sendMessage($chat_id, $text, 'html');
                            }
                        }
                    }
                    $db->resetStatus($chat_id);
                    break;
                case 'search-keyword':
                    $posts = $db->searchByKeyword($userText);
                    if ($posts) {
                        $text = '';
                        foreach ($posts as $post) {
                            $text .= $helper->generate_telegram_post(get_permalink($post->ID),
                                    $post->post_title, $post->post_content) . "\n";
                        }
                        $db->resetStatus($chat_id);
                        $client->sendMessage($chat_id, $text, 'html');
                    } else {
                        $text = 'The search did not give a result.';
                        $db->resetStatus($chat_id);
                        $client->sendMessage($chat_id, $text, 'html');
                    }
                    break;
                case 'admin-post':
                    $postContent = explode('::', $userText);
                    if (count($postContent) == 2) {
                        $postData = [
                            'post_status' => 'publish',
                            'post_author' => 1,
                            'post_title' => $postContent[0],
                            'post_content' => $postContent[1],
                        ];
                        $text = 'You are awesome! <b>Post was created</b>';
                        $client->sendMessage($chat_id, $text, 'html');

                        $newPost = wp_insert_post($postData);
                        foreach ($db->chatAll() as $id) {
                            $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                                [
                                    [
                                        ['text' => 'Show at the site', 'url' => 'test']
                                    ]
                                ]
                            );
                            $text = $helper->generate_telegram_post(get_permalink($newPost),
                                $postData['post_title'], $postData['post_content']);
                            $client->sendMessage($id->chat_id, $text, 'html', false, null, $keyboard);
                        }
                        $db->updateStatus($chat_id, 'start');
                    } else {
                        $text = 'Incorrect delimiter, please re-type <b>data( example - TITLE :: BODY)</b>';
                        $client->sendMessage($chat_id, $text, 'html');
                    }
                    break;
                default:
                    $text = 'I don`t understand you. Type /help for commands list.';
                    $client->sendMessage($chat_id, $text, 'html');
                    break;
            }
        }, function ($update) use ($db, $client) {
            if ($db->is_user_exists($update->getMessage()->getChat()->getId())) {
                return true;
            } else {
                $text = 'You are not in bot database. Type: /start for starting work with bot. Type: /cancel for interrupting current command';
                $client->sendMessage($update->getMessage()->getChat()->getId(), $text);
                return false;
            }
        });
    }
}
