<?php

namespace TelegramNotifier\TelegramBot\Commands;


use TelegramBot\Api\Client;
use TelegramBot\Api\Types\Update;
use TelegramNotifier\TelegramBot\CommandProcessor;

interface CommandInterface
{
    public function make(Client $telegram, $arguments, Update $update, CommandProcessor $processor);

    public function handle($arguments);
}